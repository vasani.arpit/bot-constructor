const { Client } = require('./index');
const config = require('./config');

const client = new Client({ puppeteer: { headless: false } });

client.initialize();

client.on('qr', qr => {
  console.log('QR RECEIVED', qr);
});

client.on('authenticated', () => {
  console.log('AUTHENTICATED');
});

client.on('ready', () => {
  console.log('READY');
});

client.on('message', async msg => {
  console.log('MESSAGE RECEIVED', msg);

  for (const command of config.commands) {
    if (msg.body == command.trigger) {
      for (const answer of command.answers) {
        await client.sendMessage(msg.from, answer);
      }
      return;
    }
  }

  // COMMANDS -----
  if (msg.body == '!ping reply') {
    msg.reply('pong'); // Send a new message as a reply to the current one
  } else if (msg.body == '!ping') {
    client.sendMessage(msg.from, 'pong'); // Send a new message to the same chat
  } else if (msg.body.startsWith('!echo ')) {
    msg.reply(msg.body.slice(6)); // Replies with the same message
  } else if (msg.body.startsWith('!subject ')) {
    // Change the group subject
    let chat = await msg.getChat();
    if (chat.isGroup) {
      let newSubject = msg.body.slice(9);
      chat.setSubject(newSubject);
    } else {
      msg.reply('This command can only be used in a group!');
    }
  } else if (msg.body.startsWith('!desc ')) {
    // Change the group description
    let chat = await msg.getChat();
    if (chat.isGroup) {
      let newDescription = msg.body.slice(6);
      chat.setDescription(newDescription);
    } else {
      msg.reply('This command can only be used in a group!');
    }
  } else if (msg.body == '!leave') {
    // Leave the group
    let chat = await msg.getChat();
    if (chat.isGroup) {
      chat.leave();
    } else {
      msg.reply('This command can only be used in a group!');
    }
  } else if (msg.body == '!groupinfo') {
    let chat = await msg.getChat();
    if (chat.isGroup) {
      msg.reply(`
                *Group Details*
                Name: ${chat.name}
                Description: ${chat.description}
                Created At: ${chat.createdAt.toString()}
                Created By: ${chat.owner.user}
                Participant count: ${chat.participants.length}
            `);
    } else {
      msg.reply('This command can only be used in a group!');
    }
  } else {
    await client.sendMessage(msg.from, config.stockMessage); // Send stock message from config

    for (const command of config.commands) {
      await client.sendMessage(msg.from, command.description); // Send Commands description from config
    }
  }
});

client.on('disconnected', () => {
  console.log('Client was logged out');
});
